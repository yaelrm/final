import { City } from './interfaces/city';
import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  citiesCollection:AngularFirestoreCollection = this.db.collection('users'); 

  constructor(private db:AngularFirestore) { }

  getCities() {
    return this.citiesCollection.valueChanges({ idField: 'id'});
  }

  addCity(city: any) {
    return this.citiesCollection.add(city);
  }

  saveData(city: City) {
    return this.citiesCollection.doc(city.id).update(city);
  }

  deleteCity(cityId: string) {
    return this.citiesCollection.doc(cityId).delete();
  }


}
