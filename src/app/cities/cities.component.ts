import { AwsService } from './../aws.service';
import { AuthService } from '../auth.service';
import { CitiesService } from './../cities.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WeatherService } from '../weather.service';
import { City } from '../interfaces/city';


@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {
  displayedColumns: string[] = ['name', 'data', 'prediction', 'actions'];
  cities = [];

  form: FormGroup;
  citiesSub: any;

  user: any;
  userSub: any;

  constructor(private citiesService: CitiesService,
              private weatherService: WeatherService,
              private authService: AuthService,
              private awsService: AwsService,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.citiesSub = this.citiesService.getCities().subscribe((citiesData:any) => {
      this.cities = citiesData;
    });

    this.form = this.formBuilder.group({
      name: ['', Validators.required]
    });

    this.userSub = this.authService.getUser().subscribe(user => {
      this.user = user;
    })
  }

  addCity() {
    if (!this.form.valid) {
      return alert('Please enter the city name');
    };
    const cityName = this.form.value.name;
    for (let i=0; i<this.cities.length; i++) {
      if (this.cities[i].name == cityName) {
        return alert('This city is already exist!');
      }
    }
    const newCity = {
      name: this.form.value.name,
      email: this.user.email
    }
    this.citiesService.addCity(newCity).then(() => {
      return alert('City has been added successfully!')
    })
  }

  getData(city: City) {
    this.weatherService.searchWeatherData(city.name).subscribe((data) => {
      city.data = data;
    })
  }

  predict(city: City) {
    this.awsService.predict(city.data.temperature).subscribe((data: { result: string }) => {
      city.predict = data.result === 'true' ? 'Will Rain' : 'Will Not Rain';
    });
  }

  saveData(city: City) {
    this.citiesService.saveData(city).then(() => {
      return alert('City data saved Successfully!')
    })
  }

  deleteCity(id: string) {
    this.citiesService.deleteCity(id).then(() => {
      return alert('City data was Deleted.')
    })
  }

}
