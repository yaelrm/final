export interface City {
    id:string,
    email: string,
    name?: string,
    data?:any,
    predict: string;        
}
