import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AwsService {

  awsEndpoint = 'https://t0j11bl34f.execute-api.us-east-1.amazonaws.com/beta';

  constructor(private httpClient: HttpClient) { }

  predict(temperature) {
    return this.httpClient.post(this.awsEndpoint, {temperature})
  }
}
