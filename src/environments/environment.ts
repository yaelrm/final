// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB8Vots1Zr6iwReldxioGN6uhdlsgJOEDY",
    authDomain: "hello1-jce21.firebaseapp.com",
    projectId: "hello1-jce21",
    storageBucket: "hello1-jce21.appspot.com",
    messagingSenderId: "211561813545",
    appId: "1:211561813545:web:f45e057f373cbabea16e38"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
